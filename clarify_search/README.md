Clarify Search
==============

Installation
------------

Create the `sites/all/libraries/clarify-player` directory.

Download and extract [Clarify's player](https://github.com/Clarify/clarify-audio-player) as follows:

    sites/all/libraries/clarify-player/
    |- img/
    |  |- control-play-loading.gif
    |  |- player-background.png
    |  `- player-controls.png
    |- Jplayer.swf
    |- jquery.jplayer-2.2.0.min.js
    |- o3v-player.css
    `- o3v_player.js

