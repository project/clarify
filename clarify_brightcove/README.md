Clarify Brightcove
==================


Requirements
------------

* [Brightcove Video Connect](https://www.drupal.org/project/brightcove) version 7.x-6.2 or higher.
  * The __Track video ingestion__ option under _admin/config/media/brightcove_ must be selected and the callback URL must be accessible from the internet (public IP address or port-forwarding enabled).


Testing
-------

On the configuration page: _admin/config/services/clarify_
1. Enter your Clarify.io API key
2. Enable __Verbose logging__
3. Enter a __Mock video url__ for example: `video/presentations/SirKenRobinson-TED2006-How-Schools-Kill-Creativity.mp4`
4. Select __Generate captions using Clarify.io__ when uploading a Brightcove video