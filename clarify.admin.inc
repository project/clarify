<?php

/**
 * @file
 * Admin functions for the Clarify API module.
 */

function clarify_form_admin() {
  $form = [];
  $form['clarify_key'] = [
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('API key'),
    '#description' => t('Enter your Clarify.io API key. Please note that !charges may apply.', [
      '!charges' => l(t('charges'), 'http://clarify.io/pricing/', [
        'attributes' => ['target' => '_blank']
      ]),
    ]),
    '#default_value' => variable_get('clarify_key', ''),
  ];

  $form['development'] = [
    '#type' => 'fieldset',
    '#title' => t('Development settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['development']['clarify_verbose'] = [
    '#type' => 'checkbox',
    '#title' => t('Verbose logging'),
    '#default_value' => variable_get('clarify_verbose'),
  ];

  $form['development']['clarify_mock_video_url'] = [
    '#type' => 'textfield',
    '#title' => t('Mock video url'),
    '#field_prefix' => 'http://media.clarify.io/',
    '#description' => t('Overwrites all video URLs in the outgoing requests to clarify.'),
    '#default_value' => variable_get('clarify_mock_video_url'),
  ];

  return system_settings_form($form);
}
