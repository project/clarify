Clarify API
===========

Installation
------------

    mkdir -p sites/all/libraries/clarify-php
    cd sites/all/modules/contrib/clarify
    composer install
    mv vendor ../../../libraries/clarify-php
    drush en clarify
