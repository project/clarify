(function ($) {

/**
 * Provide the player(s) for the Clarify (search) results.
 */
Drupal.behaviors.clarifySearch = {
  attach: function (context) {
    o3vPlayer.jPlayerOptions.swfPath = 'sites/all/libraries/clarify-player';

    // Cycle through the search results and add a player for each.
    for (var i = 0; i < Drupal.settings.clarifySearch.length; i++) {
      var player = o3vPlayer.createPlayer('#player_instance_' + Drupal.settings.clarifySearch[i].key,
        Drupal.settings.clarifySearch[i].mediaUrl,
        Drupal.settings.clarifySearch[i].duration,
        {volume:0.5}
      );

      // If there are search results for the current item, add them as markers.
      if (Drupal.settings.clarifySearch[i].itemResult) {
        o3vPlayer.addItemResultMarkers(player,
          Drupal.settings.clarifySearch[i].duration,
          Drupal.settings.clarifySearch[i].itemResult,
          Drupal.settings.clarifySearchTerms
        );
      }
    }
  }
};

})(jQuery);
